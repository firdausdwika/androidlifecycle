package labmobile.activitylifecycle;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.os.Bundle;

/**
 * Created by Firdaus Dwika on 9/17/2015.
 */
public class TracerActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        notify("onCreate");
    }

    protected void onPause(){
        super.onPause();
        notify("onPause");
    }

    protected void onResume(){
        super.onResume();
        notify("onResume");
    }

    protected void onStop(){
        super.onStop();
        notify("onStop");
    }

    protected void onDestroy(){
        super.onDestroy();
        notify("onDestroy");
    }

    protected void onRestoreInstanceState(Bundle savedInstanceState){
        super.onRestoreInstanceState(savedInstanceState);
        notify("onRestoreInstanceState");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        notify("onSaveInstanceState");
    }

    private void notify(String methodName){
        String name = this.getClass().getName();
        String[]strings=name.split("\\.");
        Notification noti = new Notification.Builder(this).setContentTitle(methodName+" "+strings[strings.length -1]).setAutoCancel(true)
                .setSmallIcon(R.mipmap.ic_launcher).setContentText(name).build();
        NotificationManager notificationManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify((int)System.currentTimeMillis(), noti);
    }
}
