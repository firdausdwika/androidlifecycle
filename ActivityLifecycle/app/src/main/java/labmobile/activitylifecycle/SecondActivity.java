package labmobile.activitylifecycle;

import android.os.Bundle;

/**
 * Created by Firdaus Dwika on 9/17/2015.
 */
public class SecondActivity extends TracerActivity {

    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
    }
}
